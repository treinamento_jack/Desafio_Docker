package main

import (
	"fmt"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	// Ler o arquivo HTML
	html := `
<!DOCTYPE html>
<html>
<head>
    <title>Minha Pagina Estatica</title>
</head>
<body>
    <h1>esta é uma minha página estática!</h1>
    <p>ela é basica para que seja simples.</p>
</body>
</html>
`

	// Escrever o HTML na resposta HTTP
	fmt.Fprint(w, html)
}

func main() {
	http.HandleFunc("/", handler)
	fmt.Println("Running demo app. Press Ctrl+C to exit...")
	log.Fatal(http.ListenAndServe(":8888", nil))
}
